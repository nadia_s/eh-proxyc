package com.epam.mtvn.proxyc.eh.data.config;

import com.mongodb.Mongo;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 3:59
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SpringMongoConfig  extends AbstractMongoConfiguration {

    private final String DATABASE_NAME = "feed-db";
    private final String MONGO_HOST = "localhost";
    private final Integer MONGO_PORT = 27017;

    @Override
    public String getDatabaseName() {
        return DATABASE_NAME;
    }

    @Override
    public
    @Bean
    Mongo mongo() throws Exception {
        return new Mongo(MONGO_HOST);
    }

    @Override
    public
    @Bean
    MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), DATABASE_NAME);///, getUserCredentials());
    }
}
