package com.epam.mtvn.proxyc.eh.data.services;

import  com.epam.mtvn.proxyc.eh.data.model.FeedConfiguration;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 3:16
 * To change this template use File | Settings | File Templates.
 */
@Service
public class FeedConfigurationService extends AbstractService{

    public void create(FeedConfiguration feedConfiguration) {
        mongo.insert(feedConfiguration);
    }

    public List<FeedConfiguration> list() {
        List<FeedConfiguration> list = mongo.findAll(FeedConfiguration.class);
        return list;
    }

    public FeedConfiguration findOneById(String id) {
        FeedConfiguration feedConfiguration = mongo.findOne(new Query(Criteria.where(IDENTIFIER).is(id)),
                FeedConfiguration.class);
        return feedConfiguration;
    }

    public FeedConfiguration findOneByUrl(String url) {
        FeedConfiguration feedConfiguration = mongo.findOne(new Query(Criteria.where(URL_KEY).is(url)),
                FeedConfiguration.class);
        return feedConfiguration;
    }

    public List<FeedConfiguration> findManyByStatus(String status) {
        List<FeedConfiguration> feedConfigurationList = mongo.find(new Query(Criteria.where(STATUS).is(status)),
                FeedConfiguration.class);
        return feedConfigurationList;
    }

    public void updateStatus(FeedConfiguration feedConfiguration, String status) {
        mongo.updateFirst(
                new Query(Criteria.where(IDENTIFIER).is(feedConfiguration.getId())),
                Update.update(STATUS, status),
                FeedConfiguration.class);
    }

    public void update(FeedConfiguration feedConfiguration) {
        mongo.save(feedConfiguration);
    }

    public void delete(FeedConfiguration feedConfiguration) {
        mongo.remove(feedConfiguration);
    }

    public void delete(String id) {
        FeedConfiguration feedConfiguration = mongo.findOne(new Query(Criteria.where(IDENTIFIER).is(id)),
                FeedConfiguration.class);
        mongo.remove(feedConfiguration);
    }

}
