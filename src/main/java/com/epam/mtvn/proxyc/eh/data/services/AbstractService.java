package com.epam.mtvn.proxyc.eh.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 3:17
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractService {

    @Autowired
    @Qualifier(value = "mongoTemplate")
    protected MongoOperations mongo;

    protected final String IDENTIFIER = "_id";
    protected final String STATUS = "status";
    protected final String URL_KEY = "urlKey";

}
