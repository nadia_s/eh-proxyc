package com.epam.mtvn.proxyc.eh.cache.listeners;

import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerFactory;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 4:44
 * To change this template use File | Settings | File Templates.
 */
public class ProxycCacheEventListenerFactory extends CacheEventListenerFactory{
    @Override
    public CacheEventListener createCacheEventListener(Properties properties) {
        return ProxycCacheEventListener.INSTANCE;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
