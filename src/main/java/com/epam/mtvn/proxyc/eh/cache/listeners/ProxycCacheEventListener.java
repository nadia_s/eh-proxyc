package com.epam.mtvn.proxyc.eh.cache.listeners;

import com.epam.mtvn.proxyc.eh.data.services.FeedConfigurationService;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 4:26
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProxycCacheEventListener implements CacheEventListener{

    public static final CacheEventListener INSTANCE = new ProxycCacheEventListener();

    @Autowired
    FeedConfigurationService feedConfigurationService;

    @Override
    public void notifyElementRemoved(Ehcache ehcache, Element element) throws CacheException {
        feedConfigurationService.delete((String)element.getObjectKey());
        System.out.println("!!!!!!!!!!!!!!!!!!!!! Element " + element.getObjectKey() + " was removed");
    }

    @Override
    public void notifyElementPut(Ehcache ehcache, Element element) throws CacheException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void notifyElementUpdated(Ehcache ehcache, Element element) throws CacheException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void notifyElementExpired(Ehcache ehcache, Element element) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void notifyElementEvicted(Ehcache ehcache, Element element) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void notifyRemoveAll(Ehcache ehcache) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void dispose() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object clone() throws CloneNotSupportedException{
        return this;
    }
}
