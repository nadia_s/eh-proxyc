package com.epam.mtvn.proxyc.eh.data.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 3:10
 * To change this template use File | Settings | File Templates.
 */
@Document(collection = "feed-configuration-collection")
public class FeedConfiguration {

    @Id
    private String id;
    private String urlKey;
    private String status;
    private List<String> itemList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlKey() {
        return urlKey;
    }

    public void setUrlKey(String urlKey) {
        this.urlKey = urlKey;
    }

    public List<String> getItemList() {
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
