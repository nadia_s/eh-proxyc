package com.epam.mtvn.proxyc.eh.quartz.tasks;

import com.epam.mtvn.proxyc.eh.data.model.FeedConfiguration;
import com.epam.mtvn.proxyc.eh.data.services.FeedConfigurationService;
import com.epam.mtvn.proxyc.eh.json.FeedJsonParser;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Nadios
 * Date: 15.06.12
 * Time: 6:14
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UpdateMetadataTask {

    @Autowired
    FeedConfigurationService feedConfigurationService;
    @Autowired
    FeedJsonParser feedJsonParser;

    private static CacheManager cacheManager = CacheManager.create(UpdateMetadataTask.class.getResource("/ehcache.xml"));
    private static Cache cache = cacheManager.getCache("firstcache");

    public void processUpdate() {
        List<FeedConfiguration> feedConfigurationList = feedConfigurationService.findManyByStatus("NEW");
        for (FeedConfiguration feedConfiguration : feedConfigurationList) {
            Element element = cache.get(feedConfiguration.getUrlKey());
            String value = (String) element.getObjectValue();
            List<String> itemList =  feedJsonParser.getListOfItemIdsFromFeed(value);
        }
    }
}
