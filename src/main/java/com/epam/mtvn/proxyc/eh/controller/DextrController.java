package com.epam.mtvn.proxyc.eh.controller;

import com.epam.mtvn.proxyc.eh.cache.ProxycCacheManager;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Nadezhda_Sokolova
 * Date: 6/14/12
 * Time: 5:27 AM
 */
@Controller
@RequestMapping("/{jp}/{namespace}")
public class DextrController {

    @Autowired
    private ProxycCacheManager proxycCacheManager;
    
    private final static String MTVN_SERVICES_URL_START = "http://squid.jp.mtvnservices.com/";
    private final static String SLASH = "/";
    private final static String QUESTION = "?";

    @RequestMapping(method = RequestMethod.GET)
    public void method(HttpServletRequest request,
                       HttpServletResponse response,
                       @PathVariable String jp,
                       @PathVariable String namespace) throws Exception {
        String queryString = request.getQueryString();
        jp.equals(namespace);
        ServletOutputStream out = response.getOutputStream();
        String urlString = composeUrl(jp, namespace, queryString);
        String valud = proxycCacheManager.retrieveElement(urlString);
        out.println(valud);
    }
    
    private String composeUrl (String jp, String namespace, String queryString) {
        StringBuilder urlBuilder = new StringBuilder(MTVN_SERVICES_URL_START);
        urlBuilder.append(jp);
        urlBuilder.append(SLASH);
        urlBuilder.append(namespace);
        urlBuilder.append(QUESTION);
        urlBuilder.append(queryString);
        return urlBuilder.toString();
        //String urlString = "http://squid.jp.mtvnservices.com/" + jp + "/"
          //      + namespace + "?" + queryString;
    }


}
