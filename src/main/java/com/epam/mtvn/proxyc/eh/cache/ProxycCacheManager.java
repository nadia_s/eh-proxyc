package com.epam.mtvn.proxyc.eh.cache;

import com.epam.mtvn.proxyc.eh.data.model.FeedConfiguration;
import com.epam.mtvn.proxyc.eh.data.services.FeedConfigurationService;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Nadezhda_Sokolova
 * Date: 6/14/12
 * Time: 9:55 AM
 */
@Service
public class ProxycCacheManager {

    @Autowired
    FeedConfigurationService feedConfigurationService;

    private static CacheManager cacheManager = CacheManager.create(ProxycCacheManager.class.getResource("/ehcache.xml"));
    private static Cache cache = cacheManager.getCache("firstcache");

    /**
     * retrieves value  from cache if exists
     * if not  create it and add  it to cache
     */
    public String retrieveElement(String key) throws Exception{
        URL url = null;
        InputStream is = null;
        Reader reader = null;
        Element element = null;
                //get an element  from cache by key
        String value = "";

        FeedConfiguration feedConfiguration = feedConfigurationService.findOneByUrl(key);
        if (feedConfiguration == null) {
            url = new URL(key);
            is = url.openStream();
            reader = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(reader);

            String s;
            StringBuilder fullAnswer = new StringBuilder();
            while ((s = br.readLine()) != null) {
                fullAnswer.append(s);
            }
            value = fullAnswer.toString();
            cache.put(new Element(key, value));
            feedConfiguration = new FeedConfiguration();
            feedConfiguration.setUrlKey(key);
            feedConfiguration.setStatus("NEW");
            feedConfigurationService.create(feedConfiguration);
        } else {
            value = (String) element.getValue();
        }
        return value;
    }

    /**
     * refresh value  for given key
     */
    public void refresh(String key, net.sf.ehcache.Cache cache) {
        cache.remove(key);
    }

    /**
     * to call eventually  when your application is  exiting  */
}
